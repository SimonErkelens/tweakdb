<?php
require_once('classes/sqliteconnector.php');
$sqlitedb = new SimplyQLite('db/locations.db');
$select   = $sqlitedb->selectAll(SQLITE3_BOTH);
while ($list = $select->fetchArray(SQLITE3_ASSOC)) {
    if (!$list['tweakersID']) {
        $user    = $list['Name'];
        $user    = preg_replace('/\s+/', '%20', trim($user));
        $headers = get_headers('https://gathering.tweakers.net/xml/user_profile/' . $user);
        if ($headers[0] !== 'HTTP/1.0 404 Not Found') {
            $tweakUser = file_get_contents('https://gathering.tweakers.net/xml/user_profile/' . $user);
            $xml       = simplexml_load_string($tweakUser);
            $json      = json_encode($xml);
            $array     = json_decode($json, true);
            $userid    = $array['section']['output_result']['sections']['section']['user_id'];
            var_dump($list);
            $sqlitedb->update($list['Id'], $userid);
        }
    }
}