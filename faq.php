<?php
session_start();
require_once('classes/SimplyQLite.php');
if (!array_key_exists("token", $_SESSION)) {
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
$sqlitedb = new SimplyQLite('db/locations.db', 'locations', 'id');
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <title>Vraagjes beantwoord</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" type="text/css" href="css/style.css?q=<?php echo filemtime('css/style.css'); ?>">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <?php include('helpers/jsArray.php'); ?>
</head>
<body>
<div id="map-canvas"></div>
<div id="accordion" class="accordion">
    <h3>DISCLAIMER</h3>

    <div>
        <p>
            Het lezen, gebruiken of zelfs maar consumeren van enige content-pagina op deze Tweakers-map kan schade
            toebrengen aan u of anderen om u heen.
        </p>

        <p>
            Ik kan niet verantwoordelijk worden gehouden voor enige schade aan u, uw computer, uw website, uw
            kat/hond/hamster/boa constrictor/Playstation 2, of enig ander voorwerp of wezen dan ook. Mocht u schade
            ondervinden als gevolg van deze site, adviseer ik u om een psycholoog of psychiater te zoeken die u kan
            helpen met uw tekort aan gevoel voor humor.
        </p>
    </div>
    <h3>Heb jij tijd teveel ofzo?</h3>

    <div>
        <p>
            Ja.
        </p>
    </div>
    <h3>Wat sla je allemaal op?</h3>

    <div>
        <p>
            Jouw latitude/longitude, username en postcode.
        </p>
    </div>
    <h3>Hey, mijn privacy dan?!</h3>

    <div>
        <p>
            Ik verplicht je toch niet?
        </p>
    </div>
    <h3>Wie ben jij?</h3>

    <div>
        <p>
            <a href="http://firesphe.re" target="_blank">Firesphere</a>
        </p>
    </div>
    <h3>Jij bent gek</h3>

    <div>
        <p>
            Klopt, maar dat is niet echt een vraag he?
        </p>
    </div>
    <h3>Stop eens met mijn locatie te vragen!</h3>

    <div>
        <p>
            Dat is alleen maar om het "voeg-mij-toe" formulier alvast te vullen met de gegevens die je browser
            doorstuurt. Niet zo miepen.
        </p>
    </div>
    <h3>Verder alles goed?</h3>

    <div>
        <p>
            Nee, er zit een wesp in m'n werkkamer, enorm irritant.
        </p>
    </div>
    <h3>Anders nog plannen voor dit geval?</h3>

    <div>
        <p>
            Het is niet alsof je een feature-request kan sturen via <a href='/support.php' title='HELP!'>help</a> ofzo.
        </p>
    </div>
    <h3>Je moet echt feature X implementeren!</h3>

    <div>
        <p>
            Ik geloof je op je woord.
        </p>
    </div>
    <h3>Hoe de fuck sluit ik die infowindow?</h3>

    <div>
        <p>
            Anders klik je op het rode kruisje, rechtsbovenin de bubble, op de punt van de T? Wie weet helpt het.
        </p>
    </div>
    <h3>Ik wil niet meer in jouw database staan!</h3>

    <div>
        <p>
            Slinger even een mailtje mijn kant op met je username en ik haal je uit de database.
        </p>
    </div>
    <h3>Hoe kan ik jou mailen?</h3>

    <div>
        <p>
            Kom op, je bent tweaker. Een beetje creativiteit graag! Mijn e-mailadres is geen geheim ofzo!
        </p>
    </div>
    <h3>Nog verdere plannen?</h3>

    <div>
        <p>
            Op dit moment niet, maar ik denk dat ik binnenkort wel weer even een concertje ga bezoeken.
        </p>
    </div>
    <h3>Ik verveel me</h3>

    <div>
        <p>
            Ja, zeg, ik ben je moeder niet! Dan verzin je maar iets om te doen.
        </p>
    </div>
    <h3>Ik ben lui en heb daardoor verkeerde coordinaten ingevoerd. Wat nu?</h3>

    <div>
        <p>
            Als je nog geen wachtwoord hebt voor je <a href="/account.php">account</a>, stuur me even een mailtje.
        </p>

        <p>
            Als je al wel een wachtwoord hebt voor je <a href="/account.php">account</a>, ben je wel heel erg lui.
        </p>

        <p>
            Als je nog geen wachtwoord voor de <a href="/account.php">account</a>-bewerken dinges geval hebt, en te lui
            bent om mij dat te vragen aan te maken, dan ben ik te lui om jou uit de database te halen.
        </p>
    </div>
    <h3>Je admin is stuk!?</h3>

    <div>
        <p>
            Goh.
        </p>
    </div>
    <h3>Hoe de fuck vind ik mijn Latitude/Longitude?!</h3>

    <div>
        <p>
            Ga naar Google Maps. En dan zo:<br/>
            <img src="//tweakers.net/ext/f/InAVZtjDeNNqsjfDKlcuPNgE/full.jpg" alt="Zoek't lekker zelf uit"/><br/>
            <caption>
                <small>Met dank aan Zeror</small>
            </caption>
        </p>
    </div>
    <h3>Draait dit echt op een Raspberry Pi?</h3>

    <div>
        <p>
            Fuck yeah!<br/>
            <img
                src="//api.xively.com/v2/feeds/89326/datastreams/Extreme_CPU_Temp.png?h=200&l=CPU-temperatuur&b=true&g=true&min=0&s=1"
                alt="CPU Temperatuur"/><br/>
            <img
                src="//api.xively.com/v2/feeds/89326/datastreams/Extreme_CPU_Load.png?h=200&l=CPU-belasting&b=true&g=true&min=0&s=1"
                alt="CPU Belasting"/><br/>
            <img
                src="//api.xively.com/v2/feeds/89326/datastreams/Extreme_MEM_Use.png?h=200&l=Geheugenmisbruik&b=true&g=true&min=0&s=1"
                alt="Geheugenmisbruik"/><br/>
        </p>
    </div>
</div>
<?php include('helpers/menu.php'); ?>
<div class="copyright">&copy;opyright <a href="http://firesphe.re" target="_blank">Firesphere Development</a>&nbsp;|&nbsp;<a
        href='support.php' title='HELP'>Ondersteuning</a>&nbsp;|&nbsp;<a href="http://raspberrycolocatie.nl/"
                                                                         target="_blank">Hosted by PCextreme</a></div>
<div id="zoek"></div>
<div id="save"></div>
<div id="search"></div>
<div id="go"></div>
<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsRvTiB6EivO2H9GJczhhiUnhJIk_z9OU"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquerystuff.min.js?q=<?php echo filemtime('js/jquerystuff.min.js'); ?>"></script>
<script type="text/javascript" src="js/infobubble.min.js?q=<?php echo filemtime('js/infobubble.min.js'); ?>"></script>
<script type="text/javascript"
        src="js/markerCluster.min.js?q=<?php echo filemtime('js/markerCluster.min.js'); ?>"></script>
<script type="text/javascript" src="js/site.js?q=<?php echo filemtime('js/site.js'); ?>"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-51769272-1', 'codingplayground.nl');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</body>
</html>
