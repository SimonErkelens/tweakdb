<?php
session_start();
require_once('classes/SimplyQLite.php');
if (!array_key_exists("token", $_SESSION)) {
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
$sqlitedb = new SimplyQLite('db/locations.db', 'locations', 'id');
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <title>Wat is er allemaal veranderd</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" type="text/css" href="css/style.css?q=<?php echo filemtime('css/style.css'); ?>">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <?php include('helpers/jsArray.php'); ?>
</head>
<body>
<div id="map-canvas"></div>
<div id="accordion" class="accordion">
    <h3>1 november 2015</h3>

    <div>
        <p>
        <ul>
            <li>Update voor Google Maps.</li>
            <li>Menu beetje gefixt.</li>
        </ul>
        </p>
    </div>
    <h3>17 augustus 2014</h3>

    <div>
        <p>
        <ul>
            <li><a href="/account.php">Account bewerken</a> mogelijk gemaakt. DM of mail voor een wachtwoord!</li>
            <li>Menu een beetje anders gedaan. Ik hoop dat dit werkt.</li>
        </ul>
        </p>
    </div>
    <h3>22 juli 2014</h3>

    <div>
        <p>
        <ul>
            <li>0-day bug gefixt. Dankzij SQLite was XSS mogelijk. Met dank aan <a
                    href="https://tweakers.net/gallery/262310" target="_blank" title="Douweegbertje">Douweegbertje</a>.
            </li>
            <li>Update voor de autodeploy.</li>
        </ul>
        </p>
    </div>
    <h3>21 juli 2014</h3>

    <div>
        <p>
        <ul>
            <li>Google had wat zooi omgegooit.</li>
        </ul>
        </p>
    </div>
    <h3>22 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>StartSSL certificate toegevoegd.</li>
            <li>Footer link naar PCextreme RPi colocation toegevoegd.</li>
        </ul>
        </p>
    </div>
    <h3>17 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>Afbeelding voor hoe je je lat/lng vindt.</li>
            <li>Fixje voor scrollto als het te lang wordt.</li>
            <li>Autodeploy ingesteld.</li>
            <li>Rechtermuisknop om je locatie toe te voegen in de "addme".</li>
        </ul>
        </p>
    </div>
    <h3>14 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>Link naar de Tweaker's gallery-pagina op Tweakers.net.</li>
            <li>Zoekmarker draggable met positie-update.</li>
            <li>De toevoegen-pagina ook maar even fancy gemaakt.
                <ul>
                    <li>Collapse na zoeken, zodat je makkelijk kunt drag/droppen.</li>
                    <li>En uitvouwen na drag/drop actie.</li>
                </ul>
            </li>
            <li>Bugfixje op zoeken op naam.</li>
            <li>Browser laten crashen na admin-post. (Kutstreek, I know <img src='/images/devil.gif' alt='>:)'/>)</li>
            <li>SQLiteConnector library geupdate.</li>
            <li>Update voor het binnenhalen van de Tweakers.net ID om zo link-issues te voorkomen.</li>
        </ul>
        </p>
    </div>
    <h3>12 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>Groepering.</li>
            <li>Iets leukere sluit-knop voor de bubble.</li>
            <li>Javascript minified.</li>
            <li>Changelog op de kop gezet.</li>
            <li><a href="/support.php" title="HELP!">Support</a> toegevoegd.</li>
            <li><a href="/admin" title="This is not the admin you're looking for">Admin</a> toegevoegd (Echt waar! <img
                    src='/images/hypocrite.gif' alt='O-)'/> ).
            </li>
        </ul>
        </p>
    </div>
    <h3>11 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>Changelog toegevoegd.</li>
            <li>Groene marker voor de zoekfunctie toegevoegd.</li>
            <li>Zoekfunctie op tweaker-naam.</li>
            <li>Aantal handige buttons toegevoegd waar nodig.</li>
            <li>Pre-fill voor het formulier toegevoegd, aan de hand van navigator.location.</li>
            <li>Zoom in op de door navigator.location gegeven locatie.</li>
            <li>Git the shit! Een private git-repo.</li>
            <li>Backups: Nu elke 4 uur een incremental backup naar 2 externe servers.</li>
        </ul>
        </p>
    </div>
    <h3>10 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>CSRF token eindelijk gefixt.</li>
            <li>Scripts en CSS niet uit de cache halen als de server een nieuwere heeft.</li>
            <li>Check of je wel een tweaker bent toegevoegd.</li>
            <li>Op advies van Google, de markers laten "regenen".</li>


        </ul>
        </p>
    </div>
    <h3>9 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>FAQ toegevoegd.</li>
            <li>Google Analytics toegevoegd (sorry, stats ftw).</li>
            <li>SQLite insert verbeterd.</li>
        </ul>
        </p>
    </div>
    <h3>8 juni 2014</h3>

    <div>
        <p>
        <ul>
            <li>Eerste versie live gezet. Buggy as hell.</li>
        </ul>
        </p>
    </div>
</div>
<?php include('helpers/menu.php'); ?>
<div class="copyright">&copy;opyright <a href="http://firesphe.re" target="_blank">Firesphere Development</a>&nbsp;|&nbsp;<a
        href="http://raspberrycolocatie.nl/" target="_blank">Hosted by PCextreme</a>&nbsp;|&nbsp;Al <?php echo $i; ?>
    Tweakers
</div>
<div id="zoek"></div>
<div id="save"></div>
<div id="search"></div>
<div id="go"></div>
<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsRvTiB6EivO2H9GJczhhiUnhJIk_z9OU"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquerystuff.min.js?q=<?php echo filemtime('js/jquerystuff.min.js'); ?>"></script>
<script type="text/javascript" src="js/infobubble.min.js?q=<?php echo filemtime('js/infobubble.min.js'); ?>"></script>
<script type="text/javascript"
        src="js/markerCluster.min.js?q=<?php echo filemtime('js/markerCluster.min.js'); ?>"></script>
<script type="text/javascript" src="js/site.js?q=<?php echo filemtime('js/site.js'); ?>"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-51769272-1', 'codingplayground.nl');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</body>
</html>
