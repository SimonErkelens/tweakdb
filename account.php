<?php
session_start();
require_once('helpers/definitions.php');
require_once('classes/SimplyQLite.php');
require_once('classes/password.php');
require_once('classes/UserManager.php');
if (!array_key_exists("token", $_SESSION)) {
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
$sqlitedb    = new SimplyQLite('db/locations.db', 'locations', 'Id');
$usermanager = new UserManager($sqlitedb);
$csrf        = filter_input(INPUT_POST, 'csrf');
$data        = filter_input_array(INPUT_POST);
$currentUser = $usermanager->currentUser();
if (count($data) > 0) {
    $lat  = filter_input(INPUT_POST, 'Lat');
    $lng  = filter_input(INPUT_POST, 'Lng');
    $csrf = filter_input(INPUT_POST, 'csrf');
    if (!$currentUser) {
        $usermanager->unsetUser();
        $usermanager->getUser($data);
    } elseif (is_numeric($lat) && is_numeric($lng) && $_SESSION["token"] === $csrf) {
        unset($data['Name']);
        unset($data['tweakersID']);
        if ($data['password'] === '') {
            unset($data['password']);
        } else {
            $data['salt']     = uniqid(mt_rand(), true);
            $pwd              = $data['password'] . PEPPR . $data['salt'];
            $data['password'] = $usermanager->encryptPwd($pwd);
        }
        $sqlitedb->update($data, $currentUser['Id']);
    }
    $currentUser = $usermanager->updateUserSession();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <title>Accountje bewerken</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/style.css?q=<?php echo filemtime('css/style.css'); ?>">
    <?php include('helpers/jsArray.php'); ?>
</head>
<body class="account">
<div id="map-canvas"></div>
<form action="account.php" method='post'>
    <div id="accordion" class="accordion">
        <?php if ($currentUser) { ?>
            <h3>Mijn account</h3>
            <div>
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td><label for="name">Tweakers username</label></td>
                                    <td><input disabled="true" type="text" name="Name" id="name"
                                               value="<?php echo $currentUser['Name']; ?>"/></td>
                                </tr>
                                <tr>
                                    <td><label for="tweakid">Tweakers ID</label></td>
                                    <td><input disabled="true" type="text" name="tweakersID" id="tweakid"
                                               value="<?php echo $currentUser['tweakersID']; ?>"/></td>
                                </tr>

                                <tr>
                                    <td><label for="password">Password</label></td>
                                    <td><input type="password" name="password" id="password" placeholder=""/></td>
                                </tr>
                                <tr>
                                    <td><input type="submit" value="AUFSCHLEHEN!"/></td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" class="koe">
                            <img src="/images/hans.png" title="Koe!"/><br/>
                        </td>
                    </tr>
                </table>
            </div>
            <h3>Locatiegegevens</h3>
            <div>
                <table>
                    <tr>
                        <td class="searchform">
                            <table>
                                <tr>
                                    <td><label for="adres">Adres</label></td>
                                    <td><input type="text" name="adres" id="adres" placeholder="Mijnstraat 1"/></td>
                                </tr>
                                <tr>
                                    <td><label for="postcode">Postcode</label></td>
                                    <td><input type="text" name="Postcode" id="postcode"
                                               value="<?php echo $currentUser['Postcode']; ?>"/></td>
                                </tr>
                                <tr>
                                    <td><label for="stad">Woonplaats</label></td>
                                    <td><input type="text" name="Stad" id="stad"/></td>
                                </tr>
                                <tr>
                                    <td><label for="land">Land</label></td>
                                    <td><input type="text" name="Land" id="land"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <button id="zoek">Zoek mijn lat/lng</button>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <label for="Lat">Latitude</label>
                                    </td>
                                    <td>
                                        <input type="text" name="Lat" id="Lat"
                                               value="<?php echo $currentUser['Lat']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="Lng">Longitude</label>
                                    </td>
                                    <td>
                                        <input type="text" name="Lng" id="Lng"
                                               value="<?php echo $currentUser['Lng']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" value="AUFSCHLEHEN!"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        <?php } else { ?>
            <h3>Inloggen</h3>
            <div>
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td><label for="name">Tweakers username</label></td>
                                    <td><input type="text" name="Name" id="name" placeholder="Firesphere"/></td>
                                </tr>
                                <tr>
                                    <td><label for="password">Password</label></td>
                                    <td><input type="password" name="password" id="password" placeholder=""/></td>
                                </tr>
                                <tr>
                                    <td><input type="submit" value="EINLOGGEN!"/></td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" class="koe">
                            <img src="/images/hans.png" title="Koe!"/>
                        </td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>
    <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>"/>
</form>
<?php include('helpers/menu.php'); ?>
<div class="copyright">&copy;opyright <a href="http://firesphe.re" target="_blank">Firesphere Development</a>&nbsp;|&nbsp;<a
        href='support.php' title='HELP'>Ondersteuning</a>&nbsp;|&nbsp;<a href="http://raspberrycolocatie.nl/"
                                                                         target="_blank">Hosted by PCextreme</a></div>
<div id="zoek"></div>
<div id="save"></div>
<div id="search"></div>
<div id="go"></div>
<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsRvTiB6EivO2H9GJczhhiUnhJIk_z9OU"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquerystuff.min.js?q=<?php echo filemtime('js/jquerystuff.min.js'); ?>"></script>
<script type="text/javascript" src="js/infobubble.min.js?q=<?php echo filemtime('js/infobubble.min.js'); ?>"></script>
<script type="text/javascript"
        src="js/markerCluster.min.js?q=<?php echo filemtime('js/markerCluster.min.js'); ?>"></script>
<script type="text/javascript" src="js/site.js?q=<?php echo filemtime('js/site.js'); ?>"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-51769272-1', 'codingplayground.nl');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</body>
</html>
