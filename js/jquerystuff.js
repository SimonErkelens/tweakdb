$(document).ready(function(){
	$( ".accordion" ).accordion({
		heightStyle: "content",
		collapsible: true,
		active: false
	}).css('left', $(this).width() / 8);
	$('h3:first').click();
	$('h3').on('click', function(){
		$('#accordion').animate({
			scrollTop: $(this).offset().top
		}, 2000);
		_gaq.push(['_trackEvent', 'Accordion', $(this).html()]);
	});
	$('.grappenmaker').on('click', function(){
		alert('Ja, niet alsof je al op de FAQ bent ofzo');
		_gaq.push(['_trackEvent', 'Grapje', 'Weer een grappenmaker']);
	});
	
});
