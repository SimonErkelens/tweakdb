var mapOptions = {
	center: new google.maps.LatLng(52.080982,5.1060363),
	zoom: 8
},
map,
infowindow,
i = 0,
markers = [],
searchMarker = new google.maps.Marker({
	icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png',
	draggable: true,
	animation: google.maps.Animation.DROP
}),
geocoder = new google.maps.Geocoder(),
markerCluster,
initialLocation,
button = document.getElementById('zoek'),
savebutton = document.getElementById('save'),
search = document.getElementById('go'),
searchValue = document.getElementById('search'),
el = document.getElementById('Lat');

function initialize() {

	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	if(typeof(el) !== 'undefined' && el !== null) {
		google.maps.event.addListener(map, 'rightclick', function(event){ 
			var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
			placeSearchMarker(latlng);
		});
	}

	infowindow = new InfoBubble({
		maxWidth: 260,
		maxHeight: 181,
		map: map,
		backgroundColor: 'none',
		shadowStyle: 0,
		borderWidth: 0,
		padding: 0,
		arrowSize: 0,
		marginTop: 50
	});
	setTimeout(addMarkers(), 4000);

	if(typeof(el) !== 'undefined' && el !== null && navigator.geolocation && document.getElementsByClassName('prefill').length) {
		prefillLocation();
	}
	else if(document.getElementsByClassName('account').length) {
		var lat = document.getElementById('Lat').value;
		var lng = document.getElementById('Lng').value;
		var latlng = new google.maps.LatLng(lat, lng);
		placeSearchMarker(latlng);

	}
	
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addListener(searchMarker, 'dragend', function (marker) {
	document.getElementById('Lat').value = marker.latLng.lat();
	document.getElementById('Lng').value = marker.latLng.lng();
	if(document.getElementsByClassName('prefill').length) {
		document.getElementsByTagName('h3')[0].click();
	}
});

function addMarkers() {
	for(i=0; i < tweakers.length; i++) {
		var latlng = new google.maps.LatLng(tweakers[i][1], tweakers[i][2]);
		marker = new google.maps.Marker({
			label: tweakers[i][0],
			position: latlng,
			map: map,
			draggable: false,
			animation: google.maps.Animation.DROP,
			html: '<p class="henkit"><a href="https://tweakers.net/gallery/'+tweakers[i][3]+'" target="_blank">'+tweakers[i][0]+'</a></p>'
		});
		google.maps.event.addListener(marker, "click", function () {
			infowindow.setContent(this.html);
			infowindow.open(map, this);
		});
		markers.push(marker);
	};
	markerCluster = new MarkerClusterer(map, markers);
}

function prefillLocation() {
	navigator.geolocation.getCurrentPosition(function (position) {
		initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		geocoder.geocode({'latLng': initialLocation}, function(results, status){
			if(status === "OK") {
				for(i = 0;i <results[0].address_components.length ;i++) {
					if(results[0].address_components[i].types[0] === 'route') {
						document.getElementById('adres').value = results[0].address_components[i].long_name;					
					}
					if(results[0].address_components[i].types[0] === 'locality') {
						document.getElementById('stad').value = results[0].address_components[i].long_name;					
					}
					if(results[0].address_components[i].types[0] === 'postal_code_prefix') {
						document.getElementById('postcode').value = results[0].address_components[i].long_name;					
					}
				}
				var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				placeSearchMarker(latlng);
			}
		});
	});
}

button.addEventListener('click', function(e) {
	e.preventDefault();
	var address = document.getElementById('adres').value+' '+document.getElementById('postcode').value+' '+document.getElementById('stad').value+' '+document.getElementById('land').value;
	geocoder.geocode( { 'address': address}, function(results, status) {
		searchMarker.setMap(null);
		if (status === google.maps.GeocoderStatus.OK) {
			var location = results[0].geometry.location;
			var latlng = new google.maps.LatLng(location.lat(), location.lng());
			placeSearchMarker(latlng);
			if(document.getElementsByClassName('prefill').length) {
				document.getElementsByTagName('h3')[0].click();
			}
		} else {
		      alert("Echt, geen idee waar jij woont. Dit vind Google Maps er van: " + status);
		}
	});
	return false;
});
    
savebutton.addEventListener('click', function(e) {
	var name = document.getElementById('name').value;
	var lat = document.getElementById('Lat').value;
	if(name === "") {
		e.preventDefault();
		alert("Zonder nickname is't een beetje flauw he?");
		return false;
	}
	if(lat === "") {
		var address = document.getElementById('adres').value+' '+document.getElementById('postcode').value+' '+document.getElementById('stad').value+' '+document.getElementById('land').value;
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
			      var location = results[0].geometry.location;
			      document.getElementById('Lat').value = location.lat();
			      document.getElementById('Lng').value = location.lng();
			} else {
			      alert("Echt, geen idee waar jij woont. Dit vind Google Maps er van: " + status);
			      return false;
			}
		});
	}
});

search.addEventListener('click', function(e){
	e.preventDefault();
	if(searchValue.value.length > 3) {
		for(i = 0; i < markers.length; i++) {
			if(markers[i].label.toLowerCase() === searchValue.value.toLowerCase()) {
				map.setCenter(markers[i].getPosition());
				infowindow.setContent(markers[i].html);
				infowindow.open(map, markers[i]);
				map.setZoom(14);
			}
		}
	}
});

function placeSearchMarker(latlng) {
	var markerBounds = new google.maps.LatLngBounds();
	searchMarker.setMap(map);
	searchMarker.setPosition(latlng);
	markerBounds.extend(latlng);
	map.fitBounds(markerBounds);
	map.setZoom(14);
	document.getElementById('Lat').value = latlng.lat();
	document.getElementById('Lng').value = latlng.lng();
}