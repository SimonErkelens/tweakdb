<?php
session_start();
require_once('classes/SimplyQLite.php');
require_once('classes/mailhelper.php');
if (!array_key_exists("token", $_SESSION)) {
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
$sqlitedb = new SimplyQLite('db/locations.db', 'locations', 'id');
$features = new SimplyQLite ('db/locations.db', 'features', 'id');
$mailer   = new MailHelper();
$type     = '';
$csrf     = filter_input(INPUT_POST, 'csrf');
if ($_SESSION["token"] === $csrf && isset($_POST) && count($_POST) > 1) {
    $user    = filter_input(INPUT_POST, 'Tweaker');
    $headers = get_headers('https://gathering.tweakers.net/xml/user_profile/' . $user);
    $type    = filter_input(INPUT_POST, 'Type');
    if ($headers[0] !== 'HTTP/1.0 404 Not Found') {
        $mailer->handleSupport($_POST);
        if ($type == "feature") {
            $features->insertRow($_POST);
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <title>Heb je nou nog vragen ook?!</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/style.css?q=<?php echo filemtime('css/style.css'); ?>">
    <?php include('helpers/jsArray.php'); ?>
</head>
<body>
<div id="map-canvas"></div>
<div id="accordion" class="accordion">
    <h3>Ik had een "Eureka moment" en heb nu dus een feature-request</h3>

    <div>
        <table>
            <tr>
                <td>
                    <?php if ($type === 'feature') { ?>
                        Nou, bedankt hoor. Ik zal kijken wat ik voor je kan doen.
                    <?php } else { ?>
                        <form action="support.php" method='post'>
                            <label for="Titel">Naam van mijn idee</label><br/>
                            <input id="Titel" type="text" placeholder="Titel" name="Titel"/>
                            <br/>
                            <label for="Tweaker">Mijn Tweakersnaam</label><br/>
                            <input id="Tweaker" type="text" placeholder="Tweaker" name="Tweaker"/>
                            <br/>
                            <label for="Omschrijving">Beschrijving van mijn idee</label><br/>
                            <textarea id="Omschrijving" name="Omschrijving" cols="30" rows="10"></textarea>
                            <input type="hidden" value="feature" name="Type"/><br/>
                            <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>"/>
                            <label for="Spam">Antispam maatregel. Hoe heet mijn koe?</label><br/>
                            <input id="Spam" type="text" placeholder="Naam van mijn koe" name="Koe"/>
                            <br/>
                            <input type="submit" value="Pretty please?"/>
                        </form>
                    <?php } ?>
                </td>
                <td valign="top" class="koe">
                    <img src="/images/hans.png" title="Koe!"/><br/>

                    <p>Wist je dat eigennamen beginnen met een hoofdletter?</p>
                </td>
            </tr>
        </table>
    </div>
    <h3>Ongelofelijk hansworst! Moet je zien wat voor bug ik vond!</h3>

    <div>
        <table>
            <tr>
                <td>
                    <?php if ($type == 'bug') { ?>
                        Of't een bug is, bepaal ik wel, ok?
                    <?php } else { ?>
                        <form action="support.php" method='post'>
                            <label for="Titel">Naam van de bug</label><br/>
                            <input id="Titel" type="text" placeholder="Titel" name="Titel"/>
                            <br/>
                            <label for="Tweaker">Mijn Tweakersnaam</label><br/>
                            <input id="Tweaker" type="text" placeholder="Tweaker" name="Tweaker"/>
                            <br/>
                            <label for="Omschrijving">Beschrijving van de bug met reproductiestappen</label><br/>
                            <textarea id="Omschrijving" name="Omschrijving" cols="30" rows="10"></textarea>
                            <input type="hidden" value="bug" name="Type"/>
                            <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>"/><br/>
                            <label for="Spam">Antispam maatregel. Hoe heet mijn koe?</label><br/>
                            <input id="Spam" type="text" placeholder="Naam van mijn koe" name="Koe"/>
                            <br/>
                            <input type="submit" value="Pretty please?"/>
                        </form>
                    <?php } ?>
                </td>
                <td valign="top" class="koe">
                    <img src="/images/hans.png" title="Koe!"/><br/>

                    <p>Wist je dat eigennamen beginnen met een hoofdletter?</p>

                </td>
            </tr>
        </table>
    </div>
</div>
<?php include('helpers/menu.php'); ?>
<div class="copyright">&copy;opyright <a href="http://firesphe.re" target="_blank">Firesphere Development</a>&nbsp;|&nbsp;<a
        href='support.php' title='HELP'>Ondersteuning</a>&nbsp;|&nbsp;<a href="http://raspberrycolocatie.nl/"
                                                                         target="_blank">Hosted by PCextreme</a></div>
<div id="zoek"></div>
<div id="save"></div>
<div id="search"></div>
<div id="go"></div>
<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsRvTiB6EivO2H9GJczhhiUnhJIk_z9OU"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquerystuff.min.js?q=<?php echo filemtime('js/jquerystuff.min.js'); ?>"></script>
<script type="text/javascript" src="js/infobubble.min.js?q=<?php echo filemtime('js/infobubble.min.js'); ?>"></script>
<script type="text/javascript"
        src="js/markerCluster.min.js?q=<?php echo filemtime('js/markerCluster.min.js'); ?>"></script>
<script type="text/javascript" src="js/site.js?q=<?php echo filemtime('js/site.js'); ?>"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-51769272-1', 'codingplayground.nl');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</body>
</html>
