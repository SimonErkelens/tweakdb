<?php
session_start();
require_once('helpers/definitions.php');
require_once('classes/SimplyQLite.php');
require_once('classes/password.php');
require_once('classes/UserManager.php');
if (!array_key_exists("token", $_SESSION)) {
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
$sqlitedb    = new SimplyQLite('db/locations.db', 'locations', 'Name');
$usermanager = new UserManager($sqlitedb);
$csrf        = filter_input(INPUT_POST, 'csrf');
$data        = filter_input_array(INPUT_POST);
$currentUser = $usermanager->currentUser();
if ($currentUser && $currentUser['Id'] === 1) {
    if (array_key_exists($data['Name'])) {
        $data['rawpwd']   = Bcrypt::generatePassword();
        $data['salt']     = uniqid(mt_rand(), true);
        $pwd              = $data['rawpwd'] . PEPPR . $data['salt'];
        $data['password'] = $usermanager->encryptPwd($pwd);
        $sqlitedb->update($data, $data['Name']);
    }
    ?>
    <html>
    <head>

    </head>
    <body>
    <form action="superadmin.php" method="post" accept-charset="UTF-8">
        <label for="name">Gebruiker:</label><input type="text" name="Name" id="name"><br/>
        <input type="submit" value="Genereer en opslaan"/>
    </form>
    <?php if (count($data) > 0) {
        print_r($data) . '<br />';
    } ?>
    </body>
    </html>
    <?php
} else {
    header('Location: https://tweakers.codingplayground.nl');
}