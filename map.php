<?php
session_start();
require_once('classes/SimplyQLite.php');
if (!array_key_exists("token", $_SESSION)) {
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
$sqlitedb = new SimplyQLite('db/locations.db', 'locations', 'id');
$lat      = filter_input(INPUT_POST, 'Lat');
$lng      = filter_input(INPUT_POST, 'Lng');
$csrf     = filter_input(INPUT_POST, 'csrf');
$result   = true;
if (is_numeric($lat) && is_numeric($lng) && $_SESSION["token"] === $csrf) {
    $result            = $sqlitedb->insertRow($_POST);
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php
    if (!$result) {
        echo "<script type='text/javascript'>alert('Ik denk dat je al in de databeest staat. Neem even contact op met Firesphere.')</script>";
    }
    ?>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <title>Hier be Tweakers!</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" type="text/css" href="css/style.css?q=<?php echo filemtime('css/style.css'); ?>">
    <?php include('helpers/jsArray.php'); ?>
</head>
<body>
<div id="zoek"></div>
<div id="save"></div>
<div id="map-canvas"></div>
<div class="link">
    <ul>
        <li>Menu
            <ul>
                <li><a href="/">Home</a></li>
                <li><input type="text" id="search" placeholder="Zoek iemand" list="tweakers"/><input type="submit"
                                                                                                     value="Go!"
                                                                                                     id="go"/></li>
                <li><a href="addme.php">Stop mij ook in die database</a></li>
                <li><a href="account.php">Mijn account</a></li>
                <li><a href="faq.php">Veelgestelde vragen</a></li>
                <li><a href="changelog.php">Changelog</a></li>
                <li><a href="support.php">Support (kan stuk zijn)</a></li>
            </ul>
        </li>
    </ul>
</div>
<div class="copyright">&copy;opyright <a href="http://firesphe.re" target="_blank">Firesphere Development</a>&nbsp;|&nbsp;<a
        href="http://raspberrycolocatie.nl/" target="_blank">Hosted by PCextreme</a>&nbsp;|&nbsp;Al <?php echo $i; ?>
    Tweakers
</div>
<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsRvTiB6EivO2H9GJczhhiUnhJIk_z9OU"></script>
<script type="text/javascript" src="js/infobubble.min.js?q=<?php echo filemtime('js/infobubble.min.js'); ?>"></script>
<script type="text/javascript"
        src="js/markerCluster.min.js?q=<?php echo filemtime('js/markerCluster.min.js'); ?>"></script>
<script type="text/javascript" src="js/site.js?q=<?php echo filemtime('js/site.js'); ?>"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-51769272-1', 'codingplayground.nl');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
<datalist id="tweakers">
    <?php foreach ($sqlitedb->selectAll() as $list) {
        echo "<option value='" . $list['Name'] . "'>";
    } ?>
</datalist>
</body>
</html>
