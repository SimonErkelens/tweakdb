<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mailhelper
 *
 * @author Simon 'Sphere' Erkelens
 */
class MailHelper
{

    public function handleSupport($data)
    {
        if (strtolower($data['Koe']) == "hans") {
            $to      = "simon@casa-laguna.net";
            $subject = $data['Type'] . ": " . $data['Titel'];
            $message = $data['Tweaker'] . "heeft iets.\n" . "Namelijk:\n" . $data['Omschrijving'];
            $from    = $data['Tweaker'];
            $headers = "From:" . $from;
            mail($to, $subject, $message, $headers);
        }
    }

}