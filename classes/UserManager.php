<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserManager
 *
 * @author Simon 'Sphere' Erkelens
 */
class UserManager
{
    /** @var  SimplyQLite $sqlite */
    protected $sqlite;
    /** @var Bcrypt $password */
    protected $password;

    public function __construct($sqlite)
    {
        require_once('classes/password.php');
        $this->sqlite = $sqlite;
    }

    /**
     *
     * @param array $data
     * @return bool
     */
    public function getUser($data)
    {
        $user        = $this->sqlite->selectWhere(array('Name' => $data['Name']));
        $checkPasswd = $data['password'] . PEPPR . (isset($user[0]['salt']) ? $user[0]['salt'] : '');
        $verified    = Bcrypt::checkPassword($checkPasswd, $user[0]['password']);
        if ($verified) {
            unset($user[0][7]);
            unset($user[0]['password']);
            $_SESSION['user'] = $user[0];

            return true;
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function currentUser()
    {
        return (isset($_SESSION['user']) ? $_SESSION['user'] : false);
    }

    public function unsetUser()
    {
        unset($_SESSION['user']);
    }

    public function encryptPwd($pwd)
    {
        return Bcrypt::hashPassword($pwd);
    }

    public function updateUserSession()
    {
        if ($data = $this->currentUser()) {
            $user = $this->sqlite->selectWhere(array('Name' => $data['Name']));
            if (isset($user[0])) {
                unset($user[0][7]);
                unset($user[0]['password']);
                $_SESSION['user'] = $user[0];
                $user             = $user[0];
            }
        } else {
            $user = false;
        }

        return $user;
    }
}