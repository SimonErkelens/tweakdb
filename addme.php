<?php
session_start();
require_once('classes/SimplyQLite.php');
if (!array_key_exists("token", $_SESSION)) {
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));
}
$sqlitedb = new SimplyQLite('db/locations.db', 'locations', 'id');
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta name="robots" content="noindex">
    <title>Voeg mij eens even toe aan die database ofzo</title>
    <link rel="stylesheet" type="text/css" href="css/style.css?q=<?php echo filemtime('css/style.css'); ?>">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <?php include('helpers/jsArray.php'); ?>
</head>
<body class="prefill">
<div id="map-canvas"></div>
<div class="accordion" id="accordion">
    <h3>Voeg mij toe aan de database</h3>

    <form action="map.php" method="post" accept-charset="UTF-8">
        <table>
            <tr>
                <td class="searchform">
                    <table>
                        <tr>
                            <td><label for="name">Tweakers username</label></td>
                            <td><input type="text" name="Name" id="name" placeholder="Firesphere"/></td>
                        </tr>
                        <tr>
                            <td><label for="tweakersID">Tweakers ID (sinds de API niet meer bestaat)</label></td>
                            <td><input type="text" name="tweakersID" id="tweakersID" placeholder="12345"/></td>
                        </tr>
                        <tr>
                            <td><label for="adres">Adres</label></td>
                            <td><input type="text" name="adres" id="adres" placeholder="Mijnstraat 1"/></td>
                        </tr>
                        <tr>
                            <td><label for="postcode">Postcode</label></td>
                            <td><input type="text" name="Postcode" id="postcode" placeholder="1234 AA"/></td>
                        </tr>
                        <tr>
                            <td><label for="stad">Woonplaats</label></td>
                            <td><input type="text" name="Stad" id="stad" placeholder="Enschede"/></td>
                        </tr>
                        <tr>
                            <td><label for="land">Land</label></td>
                            <td><input type="text" name="Land" id="land" placeholder="Nederland" value="Nederland"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button id="zoek">Zoek mijn lat/lng</button>
                                <br>

                                <p><b>LET OP!</b>
                                    <br/>De voorgevulde waardes die je browser stuurt
                                    zijn vaak foutief! Controleer dit dus goed!
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <label for="Lat">Latitude</label>
                            </td>
                            <td>
                                <input type="text" name="Lat" id="Lat" placeholder="Latitude"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="Lng">Longitude</label>
                            </td>
                            <td>
                                <input type="text" name="Lng" id="Lng" placeholder="Longitude"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>"/>
                                <button id="save">Klopt, opslaan gozer!</button>
                                <p>Als het niet klopt, pas dan ff lekker zelf je latitude/longitude aan ofzo!</p>

                                <p>Je username, postcode en lat/lng worden in een sqlite database gepropt.</p>

                                <p>Je kan ook rechts-klikken en dan via drag-and-drop je marker zo goed mogelijk
                                    plaatsen.</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>
<?php include('helpers/menu.php'); ?>
<div class="copyright">&copy;opyright <a href="http://firesphe.re" target="_blank">Firesphere Development</a>&nbsp;|&nbsp;<a
        href='support.php' title='HELP'>Ondersteuning</a>&nbsp;|&nbsp;<a href="http://raspberrycolocatie.nl/"
                                                                         target="_blank">Hosted by PCextreme</a></div>
<div id="search"></div>
<div id="go"></div>
<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsRvTiB6EivO2H9GJczhhiUnhJIk_z9OU"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquerystuff.min.js?q=<?php echo filemtime('js/jquerystuff.min.js'); ?>"></script>
<script type="text/javascript" src="js/infobubble.min.js?q=<?php echo filemtime('js/infobubble.min.js'); ?>"></script>
<script type="text/javascript"
        src="js/markerCluster.min.js?q=<?php echo filemtime('js/markerCluster.min.js'); ?>"></script>
<script type="text/javascript" src="js/site.js?q=<?php echo filemtime('js/site.js'); ?>"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-51769272-1', 'codingplayground.nl');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</body>
</html>
